<?php

namespace api\controllers;

use Yii;
use common\models\DealerForm;
use backend\models\Users;
use backend\models\Employee;
use backend\models\Attendance;
use common\models\Picture;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\HttpBearerAuth;
class ApploginController extends Controller {

    protected function verbs() {
        return [
            'index' => ['POST'],
        ];
    }
  public static function allowedDomains() {
        return [
            // '*',                        // star allows all domains
            'http://localhost:8100',
            'http://localhost:8101',
            'http://localhost:8102',
            'http://localhost:8103',
            'http://localhost:8105'
        ];
    }
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' =>  static::allowedDomains(),
                // 'Origin' =>  ['http://localhost'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH',
                    'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        return $behaviors;
    }
// 
    public function actionIndex() { 
        $model = new DealerForm();
         // print_r($model);die;
        $model->username = Yii::$app->request->post('username');
        $model->password = Yii::$app->request->post('password');

        if ($model->login()) {
         $user = Yii::$app->user->identity;
         if($user->image!=''){
            $user->image = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $user->image);
         }

         $employee = Employee::find()->where(['status'=>'Active'])->all();
         $attendance = Attendance::find()->where(['status'=>'1'])->all();
           
          // $user = Frontusers::find()->where(['id' => Yii::$app->user->id])->one();
     // echo "<pre>";    print_r($user);die;
            return [

                'status' => 'success',
                'user' => $user,
                'employee' => $employee,
                'attendance' => $attendance,
            ];
        } else {
            return [
                'user' => null,
                'error' => $model->getErrors(),
                'status' => 'failed'
            ];
        }
    }
    public function actionGetcontact(){}


    
   
}
