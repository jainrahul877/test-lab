<?php

namespace api\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use backend\models\Users;
use backend\models\Employee;
use backend\models\Matchimg;
use backend\models\Attendance;
use common\models\Picture;

// $detectionfile='./detection.dat';
// $facedetectorfile='./FaceDetector.php';




// require($detectionfile);
// require($facedetectorfile);



//use backend\models\Ac;

class AppsController extends Controller {

        // public function __construct($id, $module, $config = [])
        // {
        //     \yii\helpers\VarDumper::dump([$id, $module, $config]);
        //     parent::__construct($id, $module, $config);
        //     header('Access-Control-Allow-Origin: *');
        // }

    protected function verbs() {
        return [
            'getemployeelist' => ['POST']

        ];
    }
public static function allowedDomains() {
        return [
            // '*',                        // star allows all domains
            'http://localhost:8100',
            'http://localhost:8101',
            'http://localhost:8102',
            'http://localhost:8103',
            'http://localhost:8105'
        ];
    }


 public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => static::allowedDomains(),
               // 'Origin' =>  ['http://localhost'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH',
                    'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        return $behaviors;
    }

 public function actionGetemployeelist(){

        $employee= Employee::find()->where(['status'=>'Active'])->all();

        if ($employee) {
         foreach ($employee as $value){ 
        // $employee[] = Employee::find()->where(['Img' => $value ])->asArray()->one();
         $value->Img = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $value->Img);
         // $employee->Img =$value;
         }
        }
             return [
                'data' => $employee,
                'status' => 'success'
            ];
       

    }

 public function actionSaveface(){
      header('Access-Control-Allow-Origin: *');
       
        $model= new Matchimg();
        $Base64Image = Yii::$app->request->post('image');

        if($Base64Image != ''){
                $img = str_replace('data:image/jpeg;base64,', '', $Base64Image);
                $img = str_replace('data:image/jpg;base64,', '', $img);
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);

                $data = base64_decode($img);
                $path = \yii::getAlias('@backend') . "/web/";
                $imagePath = 'resource/img/';
                $fname = time() . ".jpg";
                $filename = $imagePath . $fname;

                if (file_put_contents($path . $filename, $data)) {
                    // $model->customerImage = $imagePath . $fname;
                     $model->m_img = $imagePath . $fname;
                }
        }


       // echo "<pre>"; print_r($tracker);die;
        if($model->save(false)){
                if( $model->m_img != ''){
                   $model->m_img = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $model->m_img);
         }
               
            return [
                'data' => $model,

                'status' => 'success'
            ];
        }else{
            return [
                'data' => null,
                'status' => 'failed'
            ];
        }
    }



  


}
