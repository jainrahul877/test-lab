<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php $this->beginBody() ?>
    <body>

    <body class="hold-transition skin-red sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="<?= Url::base(); ?>" class="logo">
                    <span class="logo-mini">EICHER</span>
                    <span class="logo-lg">
                        <strong>EICHER</strong>
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <?php if (isset(Yii::$app->user->identity->image) && Yii::$app->user->identity->image != "") { ?>
                                        <img src="<?= Url::base() ."/". Yii::$app->user->identity->image; ?>" class="user-image" alt="User Image">
                                    <?php } else { ?>
                                        <img src="<?= Url::base() . "/resource/img/user.jpeg"; ?>" class="user-image" alt="User Image">

                                    <?php } ?>
                                    <span class="hidden-xs"><?= Yii::$app->user->identity->name; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">

                                        <?php if (isset(Yii::$app->user->identity->image) && Yii::$app->user->identity->image != "") { ?>
                                            <img src="<?= Url::base() ."/". Yii::$app->user->identity->image; ?>" class="img-circle" alt="User Image">
                                        <?php } else { ?>
                                            <img src="<?= Url::base() . "/resource/img/user.jpeg"; ?>" class="img-circle" alt="User Image">

                                        <?php } ?>
                                        <p>
                                            <?= Yii::$app->user->identity->username; ?>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">

                                      <div class="pull-left">
                                          <?php
                                          if (Yii::$app->user->identity->role=='DEALER') { ?>
                                              <a href="<?=  Url::toRoute(['dealers/updatepwd']); ?>" data-method="post" class="btn btn-default btn-flat">Change profile</a>
  
                                         <?php } else {?>
                                                <a href="<?=  Url::toRoute(['users/updatepwd']); ?>" data-method="post" class="btn btn-default btn-flat">Change profile</a>

                                          <?php } ?>
                                          
                                        
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?= Url::toRoute('site/logout'); ?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <?php if (isset(Yii::$app->user->identity->image) && Yii::$app->user->identity->image != "") { ?>
                                <img src="<?= Url::base() ."/". Yii::$app->user->identity->image; ?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                                <img src="<?= Url::base() . "/resource/img/user.jpeg"; ?>" class="img-circle" alt="User Image">
                            <?php } ?>

                        </div>
                        <div class="pull-left info">
                            <p><?= Yii::$app->user->identity->username; ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="<?php
                        if (Yii::$app->controller->getRoute() == "site/index") {
                            echo'active';
                        }
                        ?> treeview">
                            <a href="<?= Url::toRoute(['/']); ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "users/index" ||
                                    Yii::$app->controller->getRoute() == "users/create" ||
                                    Yii::$app->controller->getRoute() == "users/update" ||
                                    Yii::$app->controller->getRoute() == "users/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-users"></i> <span>Users</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "users/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['users/index']); ?>"><i class="fa fa-circle-o"></i> User list</a>
                                    </li>
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "users/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['users/create']); ?>"><i class="fa fa-circle-o"></i>Create User</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "dealers/index" ||
                                    Yii::$app->controller->getRoute() == "dealers/create" ||
                                    Yii::$app->controller->getRoute() == "dealers/update" ||
                                    Yii::$app->controller->getRoute() == "dealers/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-users"></i> <span>Dealers</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "dealers/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['dealers/index']); ?>"><i class="fa fa-circle-o"></i> Dealer list</a>
                                    </li>
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "dealers/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['dealers/create']); ?>"><i class="fa fa-circle-o"></i>Create Dealer</a>
                                    </li>
                                </ul>
                            </li><?php } ?>
                        <li class="treeview <?php
                        if (Yii::$app->controller->getRoute() == "customers/index" ||
                                Yii::$app->controller->getRoute() == "customers/create" ||
                                Yii::$app->controller->getRoute() == "customers/customerdetail_edit" ||
                                Yii::$app->controller->getRoute() == "customers/view") {
                            echo'active';
                        }
                        ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-users"></i> <span>Customers</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "customers/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['customers/index']); ?>"><i class="fa fa-circle-o"></i> Customer list</a>
                                </li>
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "customers/create") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['customers/create']); ?>"><i class="fa fa-circle-o"></i>Create Customer</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php
                        if (Yii::$app->controller->getRoute() == "enquiries/index" ||
                                Yii::$app->controller->getRoute() == "enquiries/create" ||
                                Yii::$app->controller->getRoute() == "enquiries/update" ||
                                Yii::$app->controller->getRoute() == "enquiries/view") {
                            echo'active';
                        }
                        ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-tag"></i> <span>Enquiries</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "enquiries/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['enquiries/index']); ?>"><i class="fa fa-circle-o"></i> Enquiries list</a>
                                </li>
                                
                                <?php
                                          if (Yii::$app->user->identity->role=='DEALER') { ?>
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "enquiries/create") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['enquiries/create']); ?>"><i class="fa fa-circle-o"></i>Create Enquiries</a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>

                        <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "models/index" ||
                                    Yii::$app->controller->getRoute() == "models/create" ||
                                    Yii::$app->controller->getRoute() == "models/update" ||
                                    Yii::$app->controller->getRoute() == "models/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-shopping-cart"></i> <span>Models</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "models/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['models/index']); ?>"><i class="fa fa-circle-o"></i> Models list</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "models/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['models/create']); ?>"><i class="fa fa-circle-o"></i>Create Models</a>
                                    </li>
                                </ul>
                            </li><?php } ?>

                        <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "services/index" ||
                                    Yii::$app->controller->getRoute() == "services/create" ||
                                    Yii::$app->controller->getRoute() == "services/update" ||
                                    Yii::$app->controller->getRoute() == "services/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-truck"></i> <span>Rewards</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "services/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['services/index']); ?>"><i class="fa fa-circle-o"></i> Rewards list</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "services/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['services/create']); ?>"><i class="fa fa-circle-o"></i>Create Rewards</a>
                                    </li>

                                </ul>
                            </li>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "tractors/index" ||
                                    Yii::$app->controller->getRoute() == "tractors/create" ||
                                    Yii::$app->controller->getRoute() == "tractors/update" ||
                                    Yii::$app->controller->getRoute() == "tractors/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-truck"></i> <span>Tractors</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "tractors/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['tractors/index']); ?>"><i class="fa fa-circle-o"></i> Tractor list</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "tractors/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['tractors/create']); ?>"><i class="fa fa-circle-o"></i>Create Tractor</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "hps/index" ||
                                    Yii::$app->controller->getRoute() == "hps/create" ||
                                    Yii::$app->controller->getRoute() == "hps/update" ||
                                    Yii::$app->controller->getRoute() == "hps/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-truck"></i> <span>Horse Power</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "hps/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['hps/index']); ?>"><i class="fa fa-circle-o"></i> HPS list</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "hps/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['hps/create']); ?>"><i class="fa fa-circle-o"></i>Create HPS</a>
                                    </li>

                                </ul>
                            </li>
                            
                            
                              <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            
                             <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "districts/index" ||
                                    Yii::$app->controller->getRoute() == "districts/create" ||
                                    Yii::$app->controller->getRoute() == "districts/update" ||
                                    Yii::$app->controller->getRoute() == "districts/view") {
                                
                               echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">

                                    <i class="fa fa-shekel"></i> <span>Districts</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "districts/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['districts/index']); ?>"><i class="fa fa-circle-o"></i>Districts List</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "districts/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['districts/create']); ?>"><i class="fa fa-circle-o"></i>Create Districts</a>
                                    </li>

                                </ul>
                            </li>
                                <?php } ?> 

                       <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            
                             <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "regions/index" ||
                                    Yii::$app->controller->getRoute() == "regions/create" ||
                                    Yii::$app->controller->getRoute() == "regions/update" ||
                                    Yii::$app->controller->getRoute() == "regions/view") {
                                
                               echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">

                                    <i class="fa fa-shekel"></i> <span>Regions</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "regions/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['regions/index']); ?>"><i class="fa fa-circle-o"></i>Region List</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "regions/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['regions/create']); ?>"><i class="fa fa-circle-o"></i>Create Region</a>
                                    </li>

                                </ul>
                            </li>
                                <?php } ?> 

                            
                            
                            
                             <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            
                             <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "districts/index" ||
                                    Yii::$app->controller->getRoute() == "districts/create" ||
                                    Yii::$app->controller->getRoute() == "districts/update" ||
                                    Yii::$app->controller->getRoute() == "districts/view") {
                                
                               echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">

                                    <i class="fa fa-shekel"></i> <span>Tehsil</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "tehsils/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['tehsils/index']); ?>"><i class="fa fa-circle-o"></i>Tehsil List</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "tehsils/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['tehsils/create']); ?>"><i class="fa fa-circle-o"></i>Create Tehsil</a>
                                    </li>

                                </ul>
                            </li>
                                <?php } ?> 

                            
                    
                    
                       <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "exmodels/index" ||
                                    Yii::$app->controller->getRoute() == "exmodels/create" ||
                                    Yii::$app->controller->getRoute() == "exmodels/update" ||
                                    Yii::$app->controller->getRoute() == "exmodels/view") {

                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">

                                    <i class="fa fa-truck"></i> <span>Exchange Models</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "exmodels/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['exmodels/index']); ?>"><i class="fa fa-circle-o"></i> Exchange Models list</a>
                                    </li>

                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "exmodels/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['exmodels/create']); ?>"><i class="fa fa-circle-o"></i>Create Exchange Models</a>
                                    </li>

                                </ul>
                            </li>
                            
                             <?php } ?> 
                                <?php } ?> 
                            <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "equipments/index" ||
                                    Yii::$app->controller->getRoute() == "equipments/create" ||
                                    Yii::$app->controller->getRoute() == "equipments/update" ||
                                    Yii::$app->controller->getRoute() == "equipments/view") {
                                echo'active';
                            }
                            ?>">
                                <a href="javascript:void(0);">
                                    <i class="fa fa-users"></i> <span>Equipments</span><i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "equipments/index") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['equipments/index']); ?>"><i class="fa fa-circle-o"></i>Equipments list</a>
                                    </li>
                                    <li class="<?php
                                    if (Yii::$app->controller->getRoute() == "equipments/create") {
                                        echo'active';
                                    }
                                    ?>">
                                        <a href="<?= Url::toRoute(['equipments/create']); ?>"><i class="fa fa-circle-o"></i>Create equipments</a>
                                    </li>
                                </ul>
                            </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?= $content ?>
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                &copy; <?= date('Y') ?> @ EICHER
                <div class="pull-right"><?= Yii::powered() ?></div>
            </footer>
        </div>
        <!-- ./wrapper -->
        <?php $this->endBody() ?>
        <?=
        $this->registerJsFile(Yii::$app->request->baseUrl . "/resource/js/moment.min.js");
        $this->registerCssFile(Yii::$app->request->baseUrl . "/resource/plugins/daterangepicker/daterangepicker.css");
        $this->registerJsFile(Yii::$app->request->baseUrl . "/resource/plugins/daterangepicker/daterangepicker.js");
        $this->registerJs("$(document).ready(function () {
                $('.datetimepicker').daterangepicker({timePicker: true, singleDatePicker: true, timePickerIncrement: 30,  locale: {
            format: 'MM/DD/YYYY h:mm A'
        }});
            });");
        ?>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
    </body>
</html>
<?php $this->endPage() ?>
