<?php
namespace api\modules\v1;

/**
 * iKargo API V1 Module
 * 
 * @author Budi Irawan <budi@ebizu.com>
 * @since 1.0
 */
class Home extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\home\home';

    public function init()
    {
        parent::init();        
    }
}
