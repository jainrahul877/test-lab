<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'resource/vendor/bootstrap/css/bootstrap.min.css',
        'resource/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        'resource/fonts/iconic/css/material-design-iconic-font.min.css',
        'resource/fonts/linearicons-v1.0.0/icon-font.min.css',
        'resource/vendor/animate/animate.css',
        'resource/vendor/css-hamburgers/hamburgers.min.css',
        'resource/vendor/animsition/css/animsition.min.css',
        'resource/vendor/select2/select2.min.css',
        'resource/vendor/daterangepicker/daterangepicker.css',
        'resource/vendor/slick/slick.css',
        'resource/vendor/MagnificPopup/magnific-popup.css',
        'resource/vendor/perfect-scrollbar/perfect-scrollbar.css',
        'resource/css/util.css',
        'resource/css/main.css',
        'resource/css/style.css',
    ];
    public $js = [
        'resource/vendor/jquery/jquery-3.2.1.min.js',
        'resource/vendor/animsition/js/animsition.min.js',
        'resource/vendor/bootstrap/js/popper.js',
        'resource/vendor/bootstrap/js/bootstrap.min.js',
        'resource/vendor/select2/select2.min.js',
        'resource/vendor/daterangepicker/moment.min.js',
        'resource/vendor/daterangepicker/daterangepicker.js',
        'resource/vendor/slick/slick.min.js',
        'resource/js/slick-custom.js',
        'resource/vendor/parallax100/parallax100.js',
        'resource/vendor/MagnificPopup/jquery.magnific-popup.min.js',
        'resource/vendor/isotope/isotope.pkgd.min.js',
        'resource/vendor/sweetalert/sweetalert.min.js',
        'resource/vendor/perfect-scrollbar/perfect-scrollbar.min.js',
        'resource/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
