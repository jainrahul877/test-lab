<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\Image;

class Picture extends Model {

    public function rules() {
        return [];
    }

    /* Make dir structure */

    public function makedirs($dirpath, $mode = 0777) {
        return is_dir($dirpath) || mkdir($dirpath, $mode, true);
    }

    /* Fetch all images of particular associated id */

    public function fetchImages($AssociatedId, $ImageType) {
        return Image::find()->where(['AssociatedID' => $AssociatedId, 'ImageType' => $ImageType])->all();
    }

    public function getBase64Images($AssociatedID, $ImageType, $Limit = false) {
        if (Yii::$app->request->get('Records')) {

            /* find all images from database of related AssociatedID and ImageType */
            $images = Image::find()->where(['ImageType' => $ImageType, 'AssociatedID' => $AssociatedID])->limit($Limit)->all();
        } else {
            $images = Image::find()->where(['ImageType' => $ImageType, 'AssociatedID' => $AssociatedID])->all();
        }
        $idata = [];
        foreach ($images as $img) {
            $AbsolutePath = Yii::$app->params['imagePath'];
            $filePath = $AbsolutePath . $img['ImagePath'];
            if (is_file($filePath)) {
                $idata[] = $this->getImageData($filePath);
            }
            //$idata[] = $img;
        }
        return $idata;
    }

    /* get base64 encoded data from image file */

 public static function getImageData1($imageFile) {

        if(is_file($imageFile) && file_exists($imageFile)){
            $type = pathinfo($imageFile, PATHINFO_EXTENSION);
            $data = file_get_contents($imageFile);
        }else{

            $img = Yii::getAlias('@backend') . "/web" ."/resource/images/users/product2.png";

            $type = pathinfo($img, PATHINFO_EXTENSION);
            $data = file_get_contents($img);
        }
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    public static function getImageData($imageFile) {

        if(is_file($imageFile) && file_exists($imageFile)){
            $type = pathinfo($imageFile, PATHINFO_EXTENSION);
            $data = file_get_contents($imageFile);
        }else{

            $img = Yii::getAlias('@backend') . "/web" ."/resource/images/users/user.jpeg";

            $type = pathinfo($img, PATHINFO_EXTENSION);
            $data = file_get_contents($img);
        }
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    public function removeImages($Images) {

        $AbsolutePath = Yii::$app->params['imagePath'];

        foreach ($Images as $imgID) {
            $Pic = Image::find()->where(['ImageID' => $imgID])->one();

            /* check image if physicall exists into dir */
            if (is_file($AbsolutePath . $Pic['ImagePath'])) {
                if(unlink($AbsolutePath . $Pic['ImagePath'])){
                    Image::findOne($imgID)->delete();
                }else{
                       return false;
                }
            }

            
        }
        return true;
    }

    /* Save image  uploaded in base64 format */

    public function saveBase64Image($ImageType, $AssociatedID, $Base64Image,
            $UserID = false) {


        /* set Base path for Associated ID */
        $AbsolutePath = Yii::$app->params['imagePath'];
        $RelativePath = $this->makeRelativePath($ImageType, $AssociatedID);

        /* create absolute upload path */
        $UploadPath = $AbsolutePath . $RelativePath;

        /* Create physical path of folders */
        $this->makedirs($UploadPath, 0777);


        // $fileindex = 1;
        $ext = ".jpg";
        /* $file = $fileindex . $ext;
          $filename = $UploadPath . "/" . $file;
          while (file_exists($filename)) {
          $fileindex++;
          $file = $fileindex . $ext;
          $filename = $UploadPath . "/" . $file;
          } */

        $file = $this->makeFileName($UploadPath);
        $filename = $UploadPath . $file . $ext;

        $img = str_replace('data:image/jpeg;base64,', '', $Base64Image);
        $img = str_replace('data:image/jpg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);


        $Uid = null;

        if ($UserID) {
            $Uid = $UserID;
        } else {
            $Uid = Yii::$app->user->identity->AirmasterUserID;
        }

        $currentTime = date('Y-m-d H:i:s');

        if (file_put_contents($filename, $data)) {

            /* Update image path into database */
            $dimg = new Image();

            $dimg->ImagePath = $RelativePath . $file . $ext;
            $dimg->ImageType = $ImageType;
            $dimg->Image = 'N/A';
            $dimg->AssociatedID = $AssociatedID;
            $dimg->CreatedBy = $Uid;
            $dimg->CreationDt = $currentTime;
            $dimg->UpdationDt = $currentTime;
            $dimg->UpdatedBy = $Uid;
            return $dimg->save();
        } else {
            return false;
        }
    }

    /* Rotate existing image */

    public function RotateSaveImage($filename, $degrees) {

        $AbsolutePath = Yii::$app->params['imagePath'];
        $filename = $AbsolutePath . $filename;
        $new_file = $filename;
        $rotang = $degrees;
        list($width, $height, $type, $attr) = getimagesize($filename);
        $size = getimagesize($filename);
        switch ($size['mime']) {
            case 'image/jpeg':
                $source = imagecreatefromjpeg($filename);
                $bgColor = imageColorAllocateAlpha($source, 0, 0, 0, 0);
                $rotation = imagerotate($source, $rotang, $bgColor);
                imagealphablending($rotation, false);
                imagesavealpha($rotation, true);
                imagecreate($width, $height);
                imagejpeg($rotation, $new_file);
                chmod($filename, 0777);

                break;
            case 'image/png':

                $source = imagecreatefrompng($filename);
                $bgColor = imageColorAllocateAlpha($source, 0, 0, 0, 0);
                $rotation = imagerotate($source, $rotang, $bgColor);
                imagealphablending($rotation, false);
                imagesavealpha($rotation, true);
                imagecreate($width, $height);
                imagepng($rotation, $new_file);
                chmod($filename, 0777);
                break;
            case 'image/gif':

                $source = imagecreatefromgif($filename);
                $bgColor = imageColorAllocateAlpha($source, 0, 0, 0, 0);
                $rotation = imagerotate($source, $rotang, $bgColor);
                imagealphablending($rotation, false);
                imagesavealpha($rotation, true);
                imagecreate($width, $height);
                imagegif($rotation, $new_file);
                chmod($filename, 0777);
                break;
        }
    }

    /* make relative path for respective Associated ID */

    public function makeRelativePath($ImageType, $AssociatedID) {

        /* Find path for related AssociatedID */
        $path = Yii::$app->db->createCommand("Exec GetImagePath @ImageType=" . $ImageType . ",@AssociatedID=" . $AssociatedID)->queryOne();

        $RelativePath = "";

        /* Create dir structure for Asset */
        if ($ImageType == "Asset") {
            if ($path['GPDebtorID'] != "" && $path['GPDebtorID'] != NULL) {
                $RelativePath = $RelativePath . $path['GPDebtorID'];
            } else {
                $RelativePath = $RelativePath . $path['CustomerID'];
            }

            if ($path['LocationCode'] != "" && $path['LocationCode'] != NULL) {
                $RelativePath = $RelativePath . "/" . $path['LocationCode'];
            } else {
                $RelativePath = $RelativePath . "/" . $path['LocationID'];
            }

            $RelativePath = $RelativePath . "/Asset/" . $path['AssetID'] . "/";
        }

        /* Create dir structure for Location */
        if ($ImageType == "Location") {
            if ($path['GPDebtorID'] != "" && $path['GPDebtorID'] != NULL) {
                $RelativePath = $RelativePath . $path['GPDebtorID'];
            } else {
                $RelativePath = $RelativePath . $path['CustomerID'];
            }

            if ($path['LocationCode'] != "" && $path['LocationCode'] != NULL) {
                $RelativePath = $RelativePath . "/" . $path['LocationCode'];
            } else {
                $RelativePath = $RelativePath . "/" . $path['LocationID'];
            }

            $RelativePath = $RelativePath . "/Location/";
        }

        /* Create dir structure for ConditionAudit */
        if ($ImageType == "ConditionAudit") {
            if ($path['GPDebtorID'] != "" && $path['GPDebtorID'] != NULL) {
                $RelativePath = $RelativePath . $path['GPDebtorID'];
            } else {
                $RelativePath = $RelativePath . $path['CustomerID'];
            }

            if ($path['LocationCode'] != "" && $path['LocationCode'] != NULL) {
                $RelativePath = $RelativePath . "/" . $path['LocationCode'];
            } else {
                $RelativePath = $RelativePath . "/" . $path['LocationID'];
            }

            $RelativePath = $RelativePath . "/Audit/" . $path['AssetID'] . "/";
        }

        return $RelativePath;
    }

    /* Upload a image */

    public function uploadImage($ImageType, $AssociatedID, $TempImage, $ext) {

        /* set Base path for Associated ID */
        $AbsolutePath = Yii::$app->params['imagePath'];

        $RelativePath = $this->makeRelativePath($ImageType, $AssociatedID);

        /* create absolute upload path */
        $UploadPath = $AbsolutePath . $RelativePath;

        /* Create physical path of folders */
        $this->makedirs($UploadPath, 0777);



        /*   $fileindex = 1;
          $file = $fileindex . $ext;
          $filename = $UploadPath . "/" . $file;
          while (file_exists($filename)) {
          $fileindex++;
          $file = $fileindex . $ext;
          $filename = $UploadPath . "/" . $file;
          }
         */
        $file = $this->makeFileName($UploadPath);

        move_uploaded_file($TempImage, $UploadPath . $file . $ext);
        return $RelativePath . $file . $ext;
    }

    public function makeFileName($UploadPath) {
        $fileindex = 1;
        $file = $fileindex;
        $filename = $UploadPath . "/" . $file;
        while (glob($filename . ".*")) {
            $fileindex++;
            $file = $fileindex;
            $filename = $UploadPath . "/" . $file;
        }

        return $file;
    }

}
