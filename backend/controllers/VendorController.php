<?php

namespace backend\controllers;

use Yii;
use backend\models\Vendor;
use backend\models\Vendordocument;
use backend\models\VendorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * VendorController implements the CRUD actions for Vendor model.
 */
class VendorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vendor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vendor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vendor();
       
        if ($model->load(Yii::$app->request->post())) {
             $documentname = Yii::$app->request->post('docname');
                // $imagses=Yii::$app->request->post('Vendor');
            // $model->save();
            $totalimg = sizeof($model->image);
            $model->name = $model->name;
            $model->email = $model->email;
            $model->mobile = $model->mobile;
            $model->vendor_catId = $model->vendor_catId;
            $model->state_id = $model->state_id;
            $model->city_id = $model->city_id;
            $model->location = $model->location;
            $model->status = $model->status;
            $model->kyc = $model->kyc;
            $model->image = $totalimg-1;
            $model->save(false);

            $vendid =  $model->vendor_id;

            $image = UploadedFile::getInstances($model,'image');
            // echo "<pre>"; print_r($model->save(false));

                   if (!is_null($image)) { 

            foreach ($image as $key => $value) {

                 $model1 = new Vendordocument();
              
               $path = realpath(Yii::$app->basePath) . "/web/";

                $img_path = "resource/img/vendordoc/"  . rand() . '.' . $value->extension;
            // echo "<pre>"; print_r($path);
                $value->saveAs($path . $img_path);
                $model1->vendorId = $vendid;
                $model1->documentimg = $img_path;
                $model1->documentName = $documentname[$key];


                $model1->save(false);
            }
                
            }
            
            return $this->redirect(['view', 'id' => $model->vendor_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
             $documentname = Yii::$app->request->post('docname');
                // $imagses=Yii::$app->request->post('Vendor');
            // $model->save();
            $totalimg = sizeof($model->image);
            $model->name = $model->name;
            $model->email = $model->email;
            $model->mobile = $model->mobile;
            $model->vendor_catId = $model->vendor_catId;
            $model->state_id = $model->state_id;
            $model->city_id = $model->city_id;
            $model->location = $model->location;
            $model->status = $model->status;
            $model->kyc = $model->kyc;
            $model->image = $totalimg-1;
            $model->save(false);

            $vendid =  $model->vendor_id;

            $image = UploadedFile::getInstances($model,'image');
            // echo "<pre>"; print_r($model->save(false));

            if (!is_null($image)) { 


                 $model1 = Vendordocument::findOne(['vendorId'=>$id]);
            if(!empty($model1)){

            foreach ($image as $key => $value) {
              $model1 = new Vendordocument();
               $path = realpath(Yii::$app->basePath) . "/web/";

                $img_path = "resource/img/vendordoc/"  . rand() . '.' . $value->extension;
            // echo "<pre>"; print_r($model1); die;
                $value->saveAs($path . $img_path);
                $model1->vendorId = $vendid;
                $model1->documentimg = $img_path;
                $model1->documentName = $documentname[$key];
                $model1->save(false);
            }
        }

            if(empty($model1)){

                foreach ($image as $key => $value) {

                $model1 = new Vendordocument();
                 $path = realpath(Yii::$app->basePath) . "/web/";

                $img_path = "resource/img/vendordoc/"  . rand() . '.' . $value->extension;
            // echo "<pre>"; print_r($path);
                $value->saveAs($path . $img_path);
                $model1->vendorId = $vendid;
                $model1->documentimg = $img_path;
                $model1->documentName = $documentname[$key];
                $model1->save(false);
            }

            }

                
            // }
                
            }
            
      
            return $this->redirect(['view', 'id' => $model->vendor_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
         $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save(false);

        return $this->redirect(['index']);
    }

     public function actionRemoveit(){
        $Id = $_POST['id'];
              $model = Vendordocument::findOne(['id' => $Id]);
        // $model->is_delete = 1;
              // echo "<pre>"; print_r($model); die;
        $model->delete();
             
              return json_encode($Id);

    }

    /**
     * Finds the Vendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vendor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
