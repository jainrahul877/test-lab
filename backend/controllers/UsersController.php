<?php

namespace backend\controllers;

use Yii;
use backend\models\Users;
use backend\models\Foodcategory;
use backend\models\Product;
use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use backend\models\Project;
use yii\web\UploadedFile;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSalesmanindex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionSamplecollectorindex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionLabteamindex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetcategorydata()
    {
        // echo "<pre>"; print_r(Yii::$app->request->post('id')); die;
        $vendorid = Yii::$app->request->post('id');
        $fcdata = Foodcategory::find()->where(['vendor_id'=>$vendorid, 'is_delete'=>0])->asArray()->all();
        // echo "<pre>"; print_r($fcdata); die;
        return json_encode($fcdata); 
    }

     public function actionGetproduct()
    {
        // echo "<pre>"; print_r(Yii::$app->request->post('id')); die;
        $vendorid = Yii::$app->request->post('id');
        // $fcdata = Foodcategory::find()->where(['vendor_id'=>$vendorid])->asArray()->all();
        $productdata = Product::find()->where(['vendorId'=>$vendorid, 'is_delete'=>0])->asArray()->all();
        // echo "<pre>"; print_r($fcdata); die;
        return json_encode($productdata); 
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate() {
       
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Users();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model, ['username']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->generateAccessToken();
            $model->repeat_password = $model->password_hash;
            // $model->depId=implode(', ', $model->depId);

           
         if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create',
                            [
                        'model' => $model,
            ]);
        }
    }

 // -------------------create salesman--------------
    public function actionCreatesalesman() {
       
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Users();
        // echo "<pre>"; print_r(Yii::$app->request->post()); die;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model, ['username']);
        }

        if ($model->load(Yii::$app->request->post())) {
             $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $path = realpath(Yii::$app->basePath) . "/web/";
                $img_path = "resource/img/users/" . rand() . '.' . $image->extension;
                $image->saveAs($path . $img_path);
                $model->image = $img_path;
            }
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->generateAccessToken();
            $model->repeat_password = $model->password_hash;
            $model->types = 2;
            // $model->depId=implode(', ', $model->depId);

           
         if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create',
                            [
                        'model' => $model,
            ]);
        }
    }
    // ----------------/create salesman-------------

    // -------------------create lab team--------------
    public function actionCreatelabteam() {
       
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Users();
        // echo "<pre>"; print_r(Yii::$app->request->post()); die;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model, ['username']);
        }

        if ($model->load(Yii::$app->request->post())) {
              $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $path = realpath(Yii::$app->basePath) . "/web/";
                $img_path = "resource/img/users/" . rand() . '.' . $image->extension;
                $image->saveAs($path . $img_path);
                $model->image = $img_path;
            }
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->generateAccessToken();
            $model->repeat_password = $model->password_hash;
            $model->types = 4;
            // $model->depId=implode(', ', $model->depId);

           
         if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create',
                            [
                        'model' => $model,
            ]);
        }
    }
    // ----------------/create lab team-------------


    // -------------------create sample collector--------------
    public function actionCreatesamplecollector() {
       
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Users();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model, ['username']);
        }

        if ($model->load(Yii::$app->request->post())) {
              $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $path = realpath(Yii::$app->basePath) . "/web/";
                $img_path = "resource/img/users/" . rand() . '.' . $image->extension;
                $image->saveAs($path . $img_path);
                $model->image = $img_path;
            }
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->generateAccessToken();
            $model->repeat_password = $model->password_hash;
            $model->types = 3;
            // $model->depId=implode(', ', $model->depId);

           
         if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create',
                            [
                        'model' => $model,
            ]);
        }
    }
    // ----------------/create sample collector-------------
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
             $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $path = realpath(Yii::$app->basePath) . "/web/";
                $img_path = "resource/img/users/" . rand() . '.' . $image->extension;
                $image->saveAs($path . $img_path);
                $model->image = $img_path;
            }
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            $model->generateAccessToken();
            $model->repeat_password = $model->password_hash;
           
         if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        }else{
            return $this->render('update', [
            'model' => $model,
        ]);
        }

        
    }
  public function actionUpdatepwd() {
    if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $id = Yii::$app->user->identity->id;
        $model = $this->findModel($id);
//        $model->scenario = 'scenariocreate';
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $path = realpath(Yii::$app->basePath) . "/web/";
                $img_path = "resource/img/users/" . $image->baseName . "_" . rand() . '.' . $image->extension;
                $image->saveAs($path . $img_path);
                $model->image = $img_path;
            }
            

            if (isset(Yii::$app->request->post()['Users']['password_hash']) && Yii::$app->request->post()['Users']['password_hash'] !=
                    "") {
                $model->setPassword($model->password_hash);
                $model->generateAuthKey();
                $model->generatePasswordResetToken();
                $model->generateAccessToken();
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update',
                            [
                        'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id,$types)
    {
        
        // $model = Users::findOne($id);
        // $model->status='0';
         $model = $this->findModel($id);
        $model->is_delete = 1;
        // $model->save(false);
       
        if($model->save(false)){
            if($types==2){
        return $this->redirect(['salesmanindex']);
    }elseif($types==3){
        return $this->redirect(['samplecollectorindex']);
    }elseif($types==4){
        return $this->redirect(['labteamindex']);
    }else{
        return $this->redirect(['index']);
    }

        }
       // $this->findModel($id)->delete();

    }

    public function actionChangestatus() 
     {
            $model = $this->findModel($_POST['id']);
           // echo "<pre>"; print_r($model);die;
            $model->status = $_POST['status'];
            $model->save(false);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // public function actionGetproject() {
    //     $getalldata = Project::find()->select('ProjectId,projectName')->where(['status' => 'Active'])->asArray()->all();
    //     return json_encode($getalldata);
    // }
}
