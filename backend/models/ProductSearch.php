<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Product;

/**
 * ProductSearch represents the model behind the search form of `backend\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'status', 'vendorId', 'foodcatid'], 'integer'],
            [['name', 'description', 'productimg','is_delete'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = Product::find()->where(['is_delete'=>0]);
        $query = Product::find()->leftJoin('vendor','product.vendorId = vendor.vendor_id')->leftJoin('foodcategory','product.foodcatid = foodcategory.food_category_id')->where(['product.is_delete'=> 0, 'foodcategory.is_delete' => 0, 'vendor.is_delete' => 0 ]);

        // $productsql = "SELECT product.*, vendor.name, foodcategory.name FROM `product` LEFT JOIN vendor on product.vendorId = vendor.vendor_id LEFT JOIN foodcategory on product.foodcatid = foodcategory.food_category_id WHERE foodcategory.is_delete = 0 AND vendor.is_delete = 0 AND product.is_delete = 0 ORDER BY product_id DESC";

        // $query = Yii::$app->db->createCommand($productsql)->queryAll();
// echo "<pre>"; print_r($query); die;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'status' => $this->status,
            'vendorId' => $this->vendorId,
            'foodcatid' => $this->foodcatid,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'productimg', $this->productimg]);

        return $dataProvider;
    }
}
