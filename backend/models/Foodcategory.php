<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "foodcategory".
 *
 * @property int $food_category_id
 * @property int $vendor_id
 * @property string $name
 * @property int $status
 */
class Foodcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foodcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'food_category_id' => 'Food Category ID',
            'vendor_id' => 'Vendor ID',
            'name' => 'Food Category Name',
            'status' => 'Status',
        ];
    }
}
