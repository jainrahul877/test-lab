<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vendor".
 *
 * @property int $vendor_id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property int $status
 * @property int $kyc
 * @property string $location
 * @property int $state_id
 * @property int $city_id
 */
class Vendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id'], 'integer'],
            [['name', 'location'], 'string', 'max' => 245],
            [['name', 'email', 'mobile', 'status', 'city_id', 'state_id','vendor_catId'], 'required'],
            [['email'], 'email'],
            // [['mobile'], 'max' => 10, 'min'=> 10],
            [['status', 'kyc'], 'string', 'max' => 4],
            [['image'], 'string', 'max' => 250],
            [['mobile'], 'integer', 'min'=> 10,],
            [['image'], 'file', 'maxFiles' => 10, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => 'Vendor ID',
            'name' => 'Vendor Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'image' => 'Image',
            'status' => 'Status',
            'kyc' => 'Kyc',
            'location' => 'Location',
            'state_id' => 'State',
            'city_id' => 'City',
            'vendor_catId' => 'Category'
        ];
    }
}
