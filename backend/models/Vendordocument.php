<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vendordocument".
 *
 * @property int $id
 * @property int $vendorId
 * @property string $documentimg
 * @property int $status 1:Active; 0:Inactive
 * @property string $createdAt
 */
class Vendordocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendordocument';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorId', 'status'], 'integer'],
            [['documentimg','documentName'], 'safe'],
            [['documentimg'], 'string', 'max' => 250],
             [['documentimg'], 'file', 'maxFiles' => 10, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorId' => 'Vendor ID',
            'documentimg' => 'Document img',
            'documentName' => 'Document Name',
            'status' => 'Status',
            'createdAt' => 'Created At',
        ];
    }
}
