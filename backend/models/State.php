<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property int $state_id
 * @property string $name
 * @property int $status
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
}
