<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $product_id
 * @property string $name
 * @property string $description
 * @property int $status
 * @property int $vendorId
 * @property int $foodcatid
 * @property string $productimg
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['productimg'], 'safe'],
            [['status', 'vendorId', 'foodcatid','is_delete'], 'integer'],
            [['name'], 'string', 'max' => 245],
            [['productimg'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'name' => 'Product Name',
            'description' => 'Description',
            'status' => 'Status',
            'vendorId' => 'Vendor ID',
            'foodcatid' => 'Foodcatid',
            'productimg' => 'Product Image',
        ];
    }
}
