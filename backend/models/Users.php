<?php

namespace backend\models;

use Yii;

use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password_hash
 * @property string $mobile
 * @property string $email
 * @property string $address
 * @property string $role
 * @property string $image
 * @property string $auth_key
 * @property string $access_token
 * @property string $password_reset_token
 * @property int $status 1:Active;0:Inactive;
 * @property string $updated_at
 * @property string $created
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
        public $repeat_password;

    public function rules()
    {
        return [
            [['name', 'email', 'gender', 'password_hash', 'mobile'], 'required'],
            [['role'], 'string'],
            [['status','depId'], 'integer'],
            [['created','loginprojectID','gender','types'], 'safe'],
            [['name', 'username', 'password_reset_token'], 'string', 'max' => 100],
            [['password_hash', 'auth_key', 'access_token'], 'string', 'max' => 500],
            [['mobile'], 'string', 'max' => 10, 'min'=> 10],
            [['email'], 'string', 'max' => 255],
            ['email', 'email'],
            [['email'], 'unique'],
            [['address'], 'string', 'max' => 200],
            [['image'], 'string', 'max' => 250],
            [['updated_at'], 'string', 'max' => 30],
            ['repeat_password', 'required', 'when' => function () {
                    return Yii::$app->controller->action->id != 'update';          
                }, 'whenClient' => "function (attribute, value) {
                        return $('#users-password_hash').val() != '';
                    }"
            ],
                ['repeat_password', 'compare', 'compareAttribute' => 'password_hash', 'skipOnEmpty' => false, 'message' => "Password doesn't match", 'when' => function () {
                    return Yii::$app->controller->action->id != 'update';
                }, 'whenClient' => "function (attribute, value) {
                return $('#users-password_hash').val() != '' ;
                }"],
        
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password_hash' => 'Password',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'address' => 'Address',
            'role' => 'Role',
            'depId' => 'Department',
            'image' => 'Image',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created' => 'Created',
            'gender' => 'Gender',
            'types' => 'Types',
        ];
    }
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateAccessToken() {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

  
}
