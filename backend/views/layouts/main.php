<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use common\models\Picture;

AppAsset::register($this);
$src = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . Yii::$app->user->identity->image);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style type="text/css">
        .content-wrapper{
    background-size: 100% 100%;
    height: 100% 100%;
    background-repeat: no-repeat;
}
    </style>
</head>
<?php $this->beginBody() ?>
<body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="<?= Url::base(); ?>" class="logo">
                    <span class="logo-mini"><strong>DCM</strong></span>
                    <span class="logo-lg">
                        <strong>DCM Test Lab</strong>
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= $src ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo ucfirst(Yii::$app->user->identity->name); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="<?= $src ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo ucfirst(Yii::$app->user->identity->name); ?>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">

                                        <div class="pull-left">
                                            
                                                <a href="<?= Url::toRoute(['users/updatepwd']); ?>" data-method="post" class="btn btn-default btn-flat">Change profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?= Url::toRoute('site/logout'); ?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= $src ?>" class="img-circle" alt="User Image" style="width:50px;height:45px;">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo ucfirst(Yii::$app->user->identity->name); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="">
                            <a href="<?= Url::toRoute(['/']); ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                      
                       <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "users/salesmanindex" ||
                            Yii::$app->controller->getRoute() == "users/samplecollectorindex" ||
                            Yii::$app->controller->getRoute() == "users/labteamindex" ||
                            Yii::$app->controller->getRoute() == "users/createsalesman" ||
                            Yii::$app->controller->getRoute() == "users/createsamplecollector" ||
                            Yii::$app->controller->getRoute() == "users/createlabteam" ||
                                    Yii::$app->controller->getRoute() == "users/create" ||
                                    Yii::$app->controller->getRoute() == "users/update" ||
                                    Yii::$app->controller->getRoute() == "users/view") {
                                echo'active';
                            }
                            ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-users"></i> <span>Employee</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "users/salesmanindex") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['users/salesmanindex']); ?>"><i class="fa fa-circle-o"></i>Salesman</a>
                                </li>
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "users/samplecollectorindex") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['users/samplecollectorindex']); ?>"><i class="fa fa-circle-o"></i> Sample Collector</a>
                                </li>
                                 <li class="<?php
                                if (Yii::$app->controller->getRoute() == "users/labteamindex") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['users/labteamindex']); ?>"><i class="fa fa-circle-o"></i> Lab Team</a>
                                </li>
                            </ul>
                        </li> 

                        


                       
                        <!-- category -->
                         <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "category/index" ||
                                Yii::$app->controller->getRoute() == "category/create" ||
                                Yii::$app->controller->getRoute() == "category/update" ||
                                Yii::$app->controller->getRoute() == "category/view") {
                                echo'active';
                            }
                            ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-list-alt"></i> <span>Category</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                               
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "category/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['category/index']); ?>"><i class="fa fa-circle-o"></i> Category list</a>
                                </li>
                                 
                            </ul>
                        </li>
                        <!-- /category -->

                        <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "vendor/index" ||
                                Yii::$app->controller->getRoute() == "vendor/create" ||
                                Yii::$app->controller->getRoute() == "vendor/update" ||
                                Yii::$app->controller->getRoute() == "vendor/view") {
                                echo'active';
                            }
                            ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-cog"></i> <span>Vendor</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "vendor/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['vendor/index']); ?>"><i class="fa fa-circle-o"></i> Vendor list</a>
                                </li>
                                 
                            </ul>
                        </li>

                        <!-- vendor end here -->
                        

                     <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "foodcategory/index" ||
                                Yii::$app->controller->getRoute() == "foodcategory/create" ||
                                Yii::$app->controller->getRoute() == "foodcategory/update" ||
                                Yii::$app->controller->getRoute() == "foodcategory/view") {
                                echo'active';
                            }
                            ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-cutlery"></i> <span>Food Category</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "foodcategory/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['foodcategory/index']); ?>"><i class="fa fa-circle-o"></i> Food Category list</a>
                                </li>
                                 
                            </ul>
                        </li>
                         <!-- /food category -->

                          <!-- product -->

                     <li class="treeview <?php
                            if (Yii::$app->controller->getRoute() == "product/index" ||
                                Yii::$app->controller->getRoute() == "product/create" ||
                                Yii::$app->controller->getRoute() == "product/update" ||
                                Yii::$app->controller->getRoute() == "product/view") {
                                echo'active';
                            }
                            ?>">
                            <a href="javascript:void(0);">
                                <i class="fa fa-cube"></i> <span>Product</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                <li class="<?php
                                if (Yii::$app->controller->getRoute() == "product/index") {
                                    echo'active';
                                }
                                ?>">
                                    <a href="<?= Url::toRoute(['product/index']); ?>"><i class="fa fa-circle-o"></i> Product list</a>
                                </li>
                                 
                            </ul>
                        </li>
                         <!-- /product -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?= $content ?>
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                
            </footer>
        </div>
        <!-- ./wrapper -->
        <?php $this->endBody() ?>
        
    </body>
</html>
<?php $this->endPage() ?>
