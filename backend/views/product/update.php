<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = 'Update Product: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">
	<div class="vendor-update">
	    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
              <?=        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">

					    <?= $this->render('_form', [
					        'model' => $model,
					    ]) ?>

 					</div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>


