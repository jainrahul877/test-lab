<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\Vendor;
use backend\models\Foodcategory;
use common\models\Picture;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?>
            <?= Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['create'], ['class' => 'fa-lg']) ?>
            
        </h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-body">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success table-responsive">
                    <div class="box-body">

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'product_id',
                                // 'name',
                                ['attribute' => 'name',
                                        'filter' => false,
                                    'label' => 'Product Name',
                                    'value' => function($model) {
                                       
                                       return $model->name ;
                                    },
                                ],
                                ['attribute' => 'vendorId',
                                    'label' => 'Vendor',
                                     'format' => 'raw',
                                    'value' => function($model) {
                                        $data =  Vendor::find()->where(['vendor_id'=>$model->vendorId, 'is_delete'=> 0])->one();
                                       
                                    if(empty($data)){
                                            $data['name'] = 'not set';
                                             return ucfirst($data['name']);
                                        }
                                        return $data['name'] ;
                                    },
                                ],
                                ['attribute' => 'foodcatid',
                                    'label' => 'Food Category',
                                     'format' => 'raw',
                                    'value' => function($model) {
                                        $data =  Foodcategory::find()->where(['food_category_id'=>$model->foodcatid, 'is_delete'=> 0])->one();
                                       
                                    if(empty($data)){
                                            $data['name'] = 'not set';
                                             return ucfirst($data['name']);
                                        }
                                        return $data['name'] ;
                                    },
                                ],
                                // 'description:ntext',
                                ['attribute' => 'description',
                                        'filter' => false,
                                    'label' => 'Description',
                                    'value' => function($model) {
                                       
                                       return $model->description ;
                                    },
                                ],
                                // 'status',
                                 ['attribute' => 'status',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } if ($model->status == 0) {
                                                return 'Inactive';
                                            }
                                        }
                                    ],
                                // 'vendorId',
                                //'foodcatid',
                                //'productimg',
                                    ['attribute' => 'productimg',
                                    'format' => 'raw',
                                    'label' => 'Image',
                                    'value' => function($model) {
                                        $src = Picture::getImageData1(Yii::getAlias('@backend') . "/web/" . $model->productimg);
                                        return '<a class="fancybox" href='.$src.'><img src='.$src.' style="width: 45px;"></a>';
                                    }
                                ],

                               ['class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:100px;'],
                                    'header' => 'Actions',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        //view button
                                        'view' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span> View',
                                                            $url,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'View'),
                                                        'class' => 'btn btn-primary btn-xs',
                                            ]);
                                        },
                                                'update' => function ($url, $model) {
                                            $url1 = Url::base() . "/product/update?id=" . $model->product_id;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit',
                                                            $url1,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-warning btn-xs',
                                            ]);
                                        },

                                         'delete' => function ($url, $model) {
                                             $url1 = Url::base() . "/product/delete?id=" . $model->product_id;
                return Html::a('<span class="glyphicon glyphicon-remove"></span> Delete',$url1, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            }
                                            ],
                                        ],
                            ],
                        ]); ?>
                    </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
<script type="text/javascript">
     $(document).ready(function() {
    $(".fancybox").fancybox();
  });
</script>