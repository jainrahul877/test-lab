<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\Vendor;
use backend\models\Foodcategory;
use common\models\Picture;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

      <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">


                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                // 'product_id',
                                'name',
                                // 'vendorId',
                                ['attribute' => 'vendorId',
                                    'label' => 'Vendor',
                                     'format' => 'raw',
                                    'value' => function($model) {
                                        $data =  Vendor::find()->where(['vendor_id'=>$model->vendorId])->one();
                                       
                                    if(empty($data)){
                                            $data['name'] = 'not set';
                                             return$data['name'];
                                        }
                                        return $data['name'] ;
                                    },
                                ],
                                // 'foodcatid',
                                ['attribute' => 'foodcatid',
                                    'label' => 'Food Category',
                                     'format' => 'raw',
                                    'value' => function($model) {
                                        $data =  Foodcategory::find()->where(['food_category_id'=>$model->foodcatid])->one();
                                       
                                    if(empty($data)){
                                            $data['name'] = 'not set';
                                             return$data['name'];
                                        }
                                        return $data['name'] ;
                                    },
                                ],
                                'description:ntext',
                                // 'status',
                                ['attribute' => 'status',
//                               'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } if ($model->status == 0) {
                                                return 'Inactive';
                                            }
                                        }
                                    ],
                                // 'productimg',
                            ],
                        ]) ?>
</div>
<div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">
                       
                             <img style="width: 150px;" class="round" src="<?=  Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $model->productimg)?>" alt="image">

                         </div>
 <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">

 <?= Html::a('Update', ['update', 'id' => $model->product_id], ['class' => 'btn btn-warning'])
                                        ?>
                                    </div>
 
            </div>
        </div>
    </section>
</div>