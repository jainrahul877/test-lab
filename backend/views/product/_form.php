<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Vendor;
use backend\models\Foodcategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use common\models\Picture;


/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
 <div class="row">
        <div class="col-xs-12">
            <div class="box-body">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="col-xs-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Product Name') ?>
            </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <?= $form->field($model, 'vendorId')->dropDownList(ArrayHelper::map(Vendor::find()->all(), 'vendor_id', 'name'), ['prompt' => 'Select Vendor', 'class' => ' form-control', 'onchange' => '$.post("' . Url::base() . '/product/getvendor?id="+$(this).val(), function(data) {
           var location =JSON.parse(data);
           var locationdata = "<option >Select Food Category</option>";
           $.each( location, function( key, value ) {
           
                 locationdata +="<option value="+value.food_category_id+">"+value.name+"</option>"
              });
               $("#product-foodcatid").html(locationdata);
        })'])->label('Vendor'); ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
             <?= $form->field($model, 'foodcatid')->dropDownList(ArrayHelper::map(Foodcategory::find()->where(['vendor_id'=>$model->vendorId])->all(), 'food_category_id', 'name'),['prompt' => 'Select Food Category'],['options'=>[$model->foodcatid=>["Selected"=>true]]])->label('Food Category'); ?>
      
            </div>
   


            <div class="col-xs-4">
                <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
            </div> 

            <div class="col-xs-4">
                <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive']) ?>
            </div>
               


                                           <div class="col-lg-4" id="uimg">
                                            <?php $src = Picture::getImageData1(Yii::getAlias('@backend') . "/web/" . $model->productimg); ?>
                                            <img id="imgupload" src="<?= $src; ?>" alt="your image "width="120" height="120" name="Product[productimg]" class="img-rounded">
                                            <input type="file" id="imgInp" onchange="document.getElementById('imgupload').src = window.URL.createObjectURL(this.files[0])" name="Product[productimg]" accept="image/*|file_extension/.jpeg,.jpg, .png,">
                                        </div>

            <div class="col-xs-12">

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgupload').attr('src', e.target.result);
            }
//            show(input);
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#imgInp').change(function () {
        readURL(this);
    });
</script>