<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!-- <?= $form->field($model, 'product_id') ?> -->

    <div class="col-lg-4">
        <?= $form->field($model, 'name') ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($model, 'description') ?>
    </div>

    <!-- <?= $form->field($model, 'status') ?> -->

    <!-- <?= $form->field($model, 'vendorId') ?> -->

    <?php // echo $form->field($model, 'foodcatid') ?>

    <?php // echo $form->field($model, 'productimg') ?>

    <div class="col-lg-4">
    <div class="form-group" style="margin-top: 24px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
