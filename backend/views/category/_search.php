<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!-- <?= $form->field($model, 'category_id') ?> -->

    <div class="col-lg-4">
        <?= $form->field($model, 'name') ?>
    </div>

    <!-- <?= $form->field($model, 'image') ?> -->

    <!-- <?= $form->field($model, 'status') ?> -->

    <!-- <?= $form->field($model, 'is_deleted') ?> -->

    <div class="col-lg-4">
    <div class="form-group" style="margin-top: 24px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
