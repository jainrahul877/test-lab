<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\Roles;
use backend\models\Department;
use common\models\Picture;


$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
                                    // 'username',
                                    'mobile',
                                    'email:email',
                                    // 'gender',
                                    ['attribute' => 'gender',
//                                         'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->gender == 1) {
                                                return 'Male';
                                            } if ($model->gender == 2) {
                                                return 'Female';
                                            }
                                        }
                                    ],
                                    // 'types',
                                    ['attribute' => 'types',
//                                         'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->types == 1) {
                                                return 'Super Admin';
                                            }
                                            if ($model->types == 2) {
                                                return 'Sales man';
                                            }
                                            if ($model->types == 3) {
                                                return 'Sample Collector';
                                            }
                                            if ($model->types == 4) {
                                                return 'Lab Team';
                                            } 
                                        }
                                    ],
                                  
                                   // [ 'attribute'=>'roleID',
                                   //    'label'=>'Role',
                                   //    'format' => 'raw',
                                   //    'value'=> Roles::findOne(['roleID'=>$model->roleID])->roleName

                                   //  ],
                                    ['attribute' => 'status',
//                                         'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } else {
                                                return 'Inactive';
                                            }
                                        }
                                    ],
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <table id="w1" class="table table-striped table-bordered detail-view">
                                <tbody>
                                    <?php if(isset($region) && $region != ""){ ?>
                                    <tr><th>Region</th>
                                        <td><?= $region->RegionName; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr><th colspan="2">Address</th></tr>
                                    <tr><td colspan="5"><?=  $model->address . " " ; ?></td></tr>
                       </tbody>
                            </table>
                        </div> 
                       <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">
                       
                             <img style="width: 150px;" class="round" src="<?=  Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $model->image)?>" alt="image">

                         </div>
                    </div>
                </div>

                <div>
                    <?= Html::a('Update', ['update', 'id' => $model->id, 'types'=> $model->types], ['class' => 'btn btn-warning'])
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
