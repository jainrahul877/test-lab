<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
// echo "<pre>"; print_r(Yii::$app->controller->getRoute()); die;
if(Yii::$app->controller->getRoute()=='users/createsalesman'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salesman'), 'url' => ['salesmanindex']];
$this->title = Yii::t('app', 'Create Salesman');
}
if(Yii::$app->controller->getRoute()=='users/create'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['index']];
$this->title = Yii::t('app', 'Create Staff');
}
if(Yii::$app->controller->getRoute()=='users/createsamplecollector'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Samplecollector'), 'url' => ['index']];
$this->title = Yii::t('app', 'Create Sample Collector');
}

if(Yii::$app->controller->getRoute()=='users/createlabteam'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labteam'), 'url' => ['index']];
$this->title = Yii::t('app', 'Create Lab Team');
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
        <?=        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(Yii::$app->controller->getRoute()=='users/createsalesman'){?>
                             <?= $this->render('_salesmanform', [
                        'model' => $model,
                        ]) ?>
                       <?php  } ?>

                       <?php if(Yii::$app->controller->getRoute()=='users/createsamplecollector'){?>
                             <?= $this->render('_samplecollectorform', [
                        'model' => $model,
                        ]) ?>
                       <?php  } ?>

                        <?php if(Yii::$app->controller->getRoute()=='users/createlabteam'){?>
                             <?= $this->render('_labteamform', [
                        'model' => $model,
                        ]) ?>
                       <?php  } ?>

                      <?php if(Yii::$app->controller->getRoute()=='users/create'){?>
                        <?= $this->render('_form', [
                        'model' => $model,
                        ]) ?>
                       <?php  } ?> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
