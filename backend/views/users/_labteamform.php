<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\models\Roles;
use backend\models\Department;
use backend\models\Users;
use common\models\Picture;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $form yii\widgets\ActiveForm */

// print_r(Yii::$app->controller->getRoute()); die;
?>

<div class="users-form">


    <div class="row">
        <div class="col-xs-12">
            <div class="box-body">
                <section>
                    <div>
                    
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="update">
                                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                                <div class="box-body">
                                    
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <?= $form->field($model, 'name')->textInput(['maxlength' => 100, 'class' => 'form-control input-sm']) ?>
                                        </div>
                                     
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <?= $form->field($model, 'gender')->dropDownList([ '1' => 'Male', '2' => 'Female']) ?>
                                        </div>
                                       
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => 10, 'class' => 'form-control input-sm', 'onkeypress' => 'return isNumberKey(event);']) ?>
                                        </div>
                                        
                                        </div>
                                        <div class="row">
                                            
                                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                                        </div>
                                           
                                   
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                                            </div>
                                     
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <?= $form->field($model, 'address')->textarea(['maxlength' => true, 'rows' => 4, 'class' => 'form-control input-sm']) ?>
                                        </div>


                                        <?php if (Yii::$app->controller->getRoute() == "users/update" || Yii::$app->controller->getRoute() == "users/updatepwd") { ?>
                                        </div>

                                    
                                    </div>
                                <?php } ?>
                                <!--</div>-->
                        


                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label" for="users-image">Image</label>

                                        <div class="col-lg-12" id="uimg">
                                            <?php $src = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $model->image); ?>
                                            <img id="imgupload" src="<?= $src; ?>" alt="your image "width="120" height="120" name="Users[image]" class="img-rounded">
                                            <input type="file" id="imgInp" onchange="document.getElementById('imgupload').src = window.URL.createObjectURL(this.files[0])" name="Users[image]" accept="image/*|file_extension/.jpeg,.jpg, .png,">
                                        </div>
                                    </div>
                               
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><hr/></div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <?=
                                        Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-warning ' : 'btn btn-warning'])
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>

                            <?php if (Yii::$app->controller->getRoute() == "users/updatepwd") { ?>

                                <div role="tabpanel" class="tab-pane" id="changepassword">
                                    <?php $form1 = ActiveForm::begin(); ?>
                                    <div class="well-lg">

                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <?= $form1->field($model, 'password_hash')->passwordInput(['maxlength' => true, 'value' => ""]) ?>

                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <?= $form1->field($model, 'repeat_password')->passwordInput(['maxlength' => true]) ?>

                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><hr/></div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <?=
                                                Html::submitButton(Yii::t('app', 'Change Password'), ['class' => 'btn btn-warning'])  
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>


</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgupload').attr('src', e.target.result);
            }
//            show(input);
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#imgInp').change(function () {
        readURL(this);
    });

    function checkFields() {
        console.log($("#users-role").val());
        var type = $("#users-role").val();
        if (type == 'OPERATOR') {
            $("#region").removeClass('hide');
            $("#users-region").select2("destroy");
            $("#users-region").select2();
        } else {
            $("#region").addClass('hide');
            //$("#users-region").val(0);

        }
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }

</script>