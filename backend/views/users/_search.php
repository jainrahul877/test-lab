<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">
   <!-- <div class="row"> -->
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!-- <?= $form->field($model, 'id') ?> -->
   
        <div class="col-lg-4">
            <?= $form->field($model, 'name') ?>
        </div>

    <!-- <?= $form->field($model, 'username') ?> -->

    <!-- <?= $form->field($model, 'password_hash') ?> -->

    <!-- <?= $form->field($model, 'auth_key') ?> -->

    <?php // echo $form->field($model, 'access_token') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>
<div class="col-lg-4">
    <div class="form-group" style="padding-top: 24px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
</div>
<!-- </div> -->
    <?php ActiveForm::end(); ?>

</div>
