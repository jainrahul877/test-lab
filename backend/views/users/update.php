<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $model backend\models\Users */


$this->title = Yii::t('app', 'Update : ', [
    'modelClass' => 'Users',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-update">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
              <?=        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!--<h3 class="box-title"/>Product</h3>-->

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(Yii::$app->request->get('types')==2){?>
                             <?= $this->render('_salesmanform', [
                        'model' => $model,
                        ]) ?>
                          <?php  } elseif(Yii::$app->request->get('types')==2){?>
                             <?= $this->render('_samplecollectorform', [
                        'model' => $model,
                        ]) ?>
                    <?php  } elseif(Yii::$app->request->get('types')==4){?>
                             <?= $this->render('_labteamform', [
                        'model' => $model,
                        ]) ?>
                       <?php  }else{ ?>
          
                        <?= $this->render('_form', [
                        'model' => $model,
                        ]) ?>
                    <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgupload').attr('src', e.target.result);
            }
//            show(input);
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#imgInp').change(function () {
        readURL(this);
    });
    
</script>