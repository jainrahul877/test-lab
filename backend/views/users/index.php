<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use backend\models\Roles;
use yii\helpers\ArrayHelper;
use common\models\Picture;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersSearch 
/* @var $dataProvider yii\data\ActiveDataProvider */

if(Yii::$app->controller->getRoute()=='users/salesmanindex'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salesman'), 'url' => ['salesmanindex']];
$this->title = Yii::t('app', 'Salesman');
$createurl = Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['createsalesman'], ['class' => 'fa-lg']);
}
if(Yii::$app->controller->getRoute()=='users/index'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['index']];
$this->title = Yii::t('app', 'Staff');
$createurl = Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['create'], ['class' => 'fa-lg']);
}
if(Yii::$app->controller->getRoute()=='users/samplecollectorindex'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Samplecollector'), 'url' => ['index']];
$this->title = Yii::t('app', 'Sample Collector');
$createurl = Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['createsamplecollector'], ['class' => 'fa-lg']);
}

if(Yii::$app->controller->getRoute()=='users/labteamindex'){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labteam'), 'url' => ['index']];
$this->title = Yii::t('app', 'Lab Team');
$createurl = Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['createlabteam'], ['class' => 'fa-lg']);
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?>
             <?= $createurl; ?>
        </h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success table-responsive">
                    <div class="box-body">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success table-responsive">
                    <div class="box-body">


                                                   <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'name',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return '<a href="view?id=' . $model->id . '">' . $model->name . '</a>';
                                    }
                                ],
                                // 'username',
                                'mobile',
                                // 'role',
                                'email:email',

                                ['attribute' => 'image',
                                    'format' => 'raw',
                                    'label' => 'Image',
                                    'value' => function($model) {
                                        $src = Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $model->image);
                                        return '<a class="fancybox" href='.$src.'><img src='.$src.' style="width: 45px;"></a>';
                                    }
                                ],

                                // ['attribute' => 'roleID',
                                // 'label'=> 'Role',
                                // 'value'=> function($model) {
                                //         $data=Yii::$app->db->createCommand("SELECT roleName FROM roles WHERE roleID=" .$model->roleID)->queryOne();
                                //         return $data['roleName'];

                                //     },
                                //     'filter' => Html::activeDropDownList($searchModel,'roleID',
                                //     ArrayHelper::map(Roles::find('roleID')->all(), 'roleID', 'roleName'),
                                //      ['class' => 'form-control', 'prompt' => 'Select All',
                                //         'id' => 'roleID',]
                                //         ),
                                
                                // ],
                               
                            //     ['attribute' => 'status',
                            //         'filter' => Html::activeDropDownList($searchModel,
                            //                 'status',
                            //                 ['1' => 'Active', '2' => 'Inactive'],
                            //                 ['class' => 'form-control', 'style' => 'width: 100%;',
                            //             'prompt' => 'Select All']),
                            //         'format' => 'raw',
                            //         'value' => function($model) {

                            //       if ($model->status == 1) {
                                    
                            //      return '<input type="checkbox" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" checked data-toggle="toggle" data-style="ios" data-size="mini" value="' . $model->id . '" id="' . $model->id . '" onchange=changestatus(this.id,"users","id","status")>';
                            //             } 
                            //             else {
                            //                 return '<input type="checkbox" data-off="Deactive" data-on="Active" data-offstyle="danger" data-onstyle="success" data-toggle="toggle" data-style="ios" data-size="mini" value="' . $model->id . '" id="' . $model->id . '" onchange=changestatus(this.id,"users","id","status")>';
                            //             }


                            // }
                            //     ],
                                ['class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:100px;'],
                                    'header' => 'Actions',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        //view button
                                        'view' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span> View',
                                                            $url,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'View'),
                                                        'class' => 'btn btn-primary btn-xs',
                                            ]);
                                        },
                                                 'update' => function ($url, $model) {
                                            $url1 = Url::base() . "/users/update?id=" . $model->id."&types=" .  $model->types;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit',
                                                            $url1,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-warning btn-xs',
                                            ]);
                                        },

                                         'delete' => function ($url, $model) {
                                             $url1 = Url::base() . "/users/delete?id=" . $model->id."&types=" .  $model->types;
                return Html::a('<span class="glyphicon glyphicon-remove"></span> Delete',$url1, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            }
                                            ],
                                        ],
                                    ],
                                ]);
                                ?>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
<script>
            function  changestatus(ths)
            {
                var id = $("#" + ths + "").val();
                console.log(id);

                if ($("#" + ths + "").is(':checked'))
                {
                    status = 1;
                }
                else
                {
                    status = 0;
                }



                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::base(); ?>" + "/users/changestatus",
            data: {id: id, status: status},
            cache: false,
            success: function (data) {

            }
        });


    }

</script>
<script type="text/javascript">
     $(document).ready(function() {
    $(".fancybox").fancybox();
  });
</script>
