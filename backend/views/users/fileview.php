<?php
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
Select image to upload:
<input type="file" name="excelfile" id="fileToUpload">
<input type="submit" value="Upload Image" name="submit">
<?php ActiveForm::end(); ?>