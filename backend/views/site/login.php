<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AppAsset;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);

$this->title = "Login";
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .login-box{
                margin: 0 auto;
                padding-top: 5%;
            }

</style>
    </head>
    <body class="hold-transition login-page" style="background-image: url(<?= Url::base() ?>/resource/images/toys/6.jpg);background-repeat: no-repeat;
    background-size: cover;
    height: auto;">
        <?php $this->beginBody() ?>

        <div class="login-box">
            <div class="login-logo" style="text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;   ">
                <!-- <img style="width:180px;" src="<?= Url::base(); ?>/resource/img/tafe.png" />  <br/> -->
               <strong> DCM </strong> |
              
                <a href="javascript:void(0);"><?= $this->title; ?></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body" style="border-radius: 10px;
    background-color: rgba(255, 255, 255, 0.81) !important;">
                <p class="login-box-msg">Sign in to start your session</p>

                <?php
                $form = ActiveForm::begin([
                            'id' => 'login-form',
                ]);
                ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?=
                $form->field($model, 'rememberMe')->checkbox()
                ?>
                <?=
                Html::submitButton('Login',
                        ['class' => 'btn btn-success btn-lg btn-block', 'name' => 'login-button'])
                ?>

<?php ActiveForm::end(); ?>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
<?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>