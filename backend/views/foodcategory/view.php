<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\Vendor;

/* @var $this yii\web\View */
/* @var $model backend\models\Foodcategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Foodcategories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foodcategory-view">

       <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'food_category_id',
            // 'vendor_id',
            // 'name',
                                ['attribute' => 'name',
                                    // 'label' => 'Vendor Name',
                                    'value' => function($model) {
                                       
                                       return ucfirst($model->name);
                                    },
                                ],
                                ['attribute' => 'vendor_id',
                                    'label' => 'Vendor Name',
                                    'value' => function($model) {
                                        $data =  Vendor::find()->where(['vendor_id'=>$model->vendor_id])->one();
                                       return ucfirst($data['name']);
                                    },
                                ],
            
            // 'status',
            ['attribute' => 'status',
//                               'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } if ($model->status == 0) {
                                                return 'Inactive';
                                            }
                                        }
                                    ],
        ],
    ]) ?>
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">

 <?= Html::a('Update', ['update', 'id' => $model->food_category_id], ['class' => 'btn btn-warning'])
                                        ?>
                                    </div>
 </div>
            </div>
        </div>
    </section>
</div>

