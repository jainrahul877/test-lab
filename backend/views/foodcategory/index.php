<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\Vendor;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FoodcategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Food Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foodcategory-index">
  <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?>
            <?= Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['create'], ['class' => 'fa-lg']) ?>
            
        </h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-body">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success table-responsive">
                    <div class="box-body">

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    // 'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'food_category_id',
                                        // 'vendor_id',
                                        // 'name',
                                        ['attribute' => 'name',
                                        'filter' => false,
                                    'label' => 'Food Category Name',
                                    'value' => function($model) {
                                       
                                       return $model->name ;
                                    },
                                ],
                                        // 'status',
                                        ['attribute' => 'vendor_id',
                                        'filter' => false,
                                    'label' => 'Vendor Name',
                                    'value' => function($model) {
                                        $data =  Vendor::find()->where(['vendor_id'=>$model->vendor_id])->one();
                                       return $data['name'];
                                    },
                                ],
                                        ['attribute' => 'status',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } if ($model->status == 0) {
                                                return 'Inactive';
                                            }
                                        }
                                    ],

                                        ['class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:100px;'],
                                    'header' => 'Actions',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        //view button
                                        'view' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span> View',
                                                            $url,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'View'),
                                                        'class' => 'btn btn-primary btn-xs',
                                            ]);
                                        },
                                                'update' => function ($url, $model) {
                                            $url1 = Url::base() . "/foodcategory/update?id=" . $model->food_category_id;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit',
                                                            $url1,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-warning btn-xs',
                                            ]);
                                        },

                                         'delete' => function ($url, $model) {
                                             $url1 = Url::base() . "/foodcategory/delete?id=" . $model->food_category_id;
                return Html::a('<span class="glyphicon glyphicon-remove"></span> Delete',$url1, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            }
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
