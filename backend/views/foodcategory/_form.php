<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Vendor;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Foodcategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foodcategory-form">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-xs-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
               <?= $form->field($model, 'vendor_id')->dropDownList(ArrayHelper::map(Vendor::find()->where(['status'=>1, 'is_delete'=>0])->all(),'vendor_id','name'),['prompt' => 'Select Vendor'])->label('vendor') ?>
            </div>
            <div class="col-xs-4">
                 <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive']) ?>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>
            </div>

         <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

