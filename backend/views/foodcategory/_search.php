<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodcategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foodcategory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!-- <?= $form->field($model, 'food_category_id') ?> -->

    <!-- <?= $form->field($model, 'vendor_id') ?> -->

    <div class="col-lg-4">
        <?= $form->field($model, 'name') ?>
    </div>

    <!-- <?= $form->field($model, 'status') ?> -->

    <div class="col-lg-4">
    <div class="form-group" style="margin-top: 24px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
