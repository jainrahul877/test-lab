<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\State;
use yii\helpers\ArrayHelper;
use backend\models\Category;
use backend\models\Vendordocument;
use kartik\file\FileInput;
use common\models\Picture;


/* @var $this yii\web\View */
/* @var $model backend\models\Vendor */
/* @var $form yii\widgets\ActiveForm */
// $model1 = new Vendordocument();
?>
<style type="text/css">
    .imgbox{
        border: 1px solid black;
        margin: 5px;
        width: 200px;
        height: 200px;
    }
    .imgbox1{
        /*border: 1px solid black;*/
        /*margin: 5px;*/
        width: 100px;
        height: 150px;
    }
</style>

<div class="vendor-form">
        <div class="row">
        <div class="col-xs-12">
            <div class="box-body">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="col-xs-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'email')->input('email') ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => 10]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'vendor_catId')->dropDownList(ArrayHelper::map(Category::find()->where(['status'=>1])->all(),'category_id','name'),['prompt' => 'Select Category'])->label('Category') ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'state_id')->dropDownList(ArrayHelper::map(State::find()->where(['status'=>1])->all(),'state_id','name'),['prompt' => 'Select State'])->label('State') ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'city_id')->textInput()->label('City') ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'location')->textArea(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
                 <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive']) ?>
            </div>
            <div class="col-xs-4"style="margin-top: 20px" >
                <!-- <?= $form->field($model, 'kyc')->textInput() ?> -->
                <?= $form->field($model, 'kyc')->checkbox(); ?>
            </div>
            <div class="col-md-12">
                <?php if (Yii::$app->controller->getRoute() == "vendor/update") { ?>
                    <!-- <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12"> -->
                        <div class="row">
                            <h4>Documents</h4>
                            
                            <hr>
                       <?php
                        $getimage = Vendordocument::find()->where(['vendorId'=>$model->vendor_id])->asArray()->all();
                       
                       foreach ($getimage as $key => $vendorimg) {
// echo "<pre>"; print_r($vendorimg['id']); die;
                       ?>

                        <div class="col-md-2 imgbox"  id="img<?= $key ?>">
                            <span class="glyphicon glyphicon-trash btn btn-danger btn-xs" style="float: right;" onclick="removeit(<?= $vendorimg['id'] ?>, <?= $key ?>)"></span>
                             <img style="width: 150px;" class="round imgbox1" src="<?=  Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $vendorimg['documentimg'])?>" alt="image"><br/>
                             <?= $vendorimg['documentName']; ?>
                         </div>
                      <?php  } ?>

                  </div>
                         <!-- </div> -->
                     <?php } ?>
            </div>
            <div class="col-xs-12" id="kycfield">
                
            <div class="form-group fieldGroup">
        <div class="input-group">
            <input type="text" id="kycname" name="docname[]" class="form-control" placeholder="Enter Document Name"/>
            <?= $form->field($model, 'image[]')->fileInput(['multiple' => true, 'class' => 'form-control', "onchange" => "chkimg(this.value)"])->label(false) ?>
          
            <div class="input-group-addon"> 
                <a href="javascript:void(0)" class="btn btn-success addMore" id="add"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true" ></span> Add</a>
            </div>
        </div>
        
    </div>

    <div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <input type="text" id="kycname" name="docname[]" class="form-control" placeholder="Enter Document Name"/>
        <?= $form->field($model, 'image[]')->fileInput(['multiple' => true, 'class' => 'form-control', "onchange" => "chkimg(this.value)"])->label(false) ?>
        
        <div class="input-group-addon"> 
            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
        </div>
        
    </div>
</div>
</div>

<div class="col-xs-12">
    <p id="error1" style="display:none; color:#FF0000;">
            Invalid Image Format! Image Format Must Be JPG, JPEG or PNG.
            </p>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    //group add limit
    var maxGroup = 10;
    
    //add more fields group
    $(".addMore").click(function(){
        $( "input" ).each(function( index ) {
  $(this).attr('id', 'item-' + index);
});
        var inpId1 = $('body').find('.fieldGroup').length;
        var inpId = "kycimg"+ inpId1;
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});

    function removeit(id, boxid){
        var imgboxid = "#img"+boxid;
        // alert(imgboxid);
        var id = id;
    if (confirm("Are you sure you want to delete this?")) {
  $.ajax({
                type: "POST",
                url: "<?= Yii::$app->urlManager->createUrl('vendor/removeit'); ?>",
                data: {id: id},
                dataType: 'json',
                cache: false,
                success: function (data) {
                    var delid = JSON.parse(data);
                    if(delid==id){
                      $(imgboxid).remove();
                    }
                    console.log(id);
                    // location.reload();

                },
            });
    }
    }
 $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
        $("#kycname").attr('required', 'required');
        $("#vendor-image").attr('required', 'required');
        // $('input[type="text"]').attr('required', 'required');
        $("#kycfield").show();
    }
    if ($(this).prop("checked") == false) {
        $("#kycname").removeAttr('required');
        $("#vendor-image").removeAttr('required');
        $("#kycfield").hide();
    }
});

 $(document).ready(function(){
             if($('input[type="checkbox"]').is(":checked")){
        $("#kycname").attr('required', 'required');
        $("#vendor-image").attr('required', 'required');
        // $('input[type="text"]').attr('required', 'required');
        $("#kycfield").show();
    }
    if ($('input[type="checkbox"]').is(":not(:checked)")) {
        $("#kycname").removeAttr('required');
        $("#vendor-image").removeAttr('required');
        $("#kycfield").hide();
    }
 });

function chkimg(path){
    var imgpath = path;
    var ext = imgpath.split(".").pop().toLowerCase();
 // var ext = $(id).val().split('.').pop().toLowerCase();
 // alert(ext);
if ($.inArray(ext, ['jpg', 'jpeg', 'png']) < 0){
 $('#error1').slideDown("slow");
 $('button:submit').attr('disabled','disabled');
 $('#add').attr('disabled','disabled');
 }else{
    $('#error1').hide("slow");
 $('button:submit').removeAttr('disabled');
 $('#add').removeAttr('disabled');

 }
}

</script>

