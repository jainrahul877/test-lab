<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\State;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-index">
    <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?>
            <?= Html::a(Yii::t('app',
                            ' <i class=\'ion-ios-plus-outline\'></i>'),
                    ['create'], ['class' => 'fa-lg']) ?>
            
        </h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                        : [],
        ])
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-body">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success table-responsive">
                    <div class="box-body">

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    // 'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'vendor_id',
                                        'name',
                                        'email',
                                        'mobile',
                                        // 'state_id',
                                        ['attribute' => 'state_id',
                                    'label' => 'State',
                                     'format' => 'raw',
                                    'value' => function($model) {
                                        $data =  State::find()->where(['state_id'=>$model->state_id])->one();
                                       
                                    if(empty($data)){
                                            $data['name'] = 'not set';
                                             return$data['name'];
                                        }
                                        return $data['name'] ;
                                    },
                                ],
                                        'city_id',

                                    //     ['attribute' => 'status',
                                    //     // 'filter' => false,
                                    //     'format' => 'raw',
                                    //     'value' => function($model) {
                                    //         if ($model->status == 1) {
                                    //             return 'Active';
                                    //         } if ($model->status == 0) {
                                    //             return 'Inactive';
                                    //         }
                                    //     }
                                    // ],
                                        //'kyc',
                                        //'location',
                                        //'state_id',

                                        ['class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => ['style' => 'width:100px;'],
                                    'header' => 'Actions',
                                    'template' => '{view} {update} {delete} {category} {product}',
                                    'buttons' => [
                                        //view button
                                        'view' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span> View',
                                                            $url,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'View'),
                                                        'class' => 'btn btn-primary btn-xs',
                                            ]);
                                        },
                                                'update' => function ($url, $model) {
                                            $url1 = Url::base() . "/vendor/update?id=" . $model->vendor_id;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Edit',
                                                            $url1,
                                                            [
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-warning btn-xs',
                                            ]);
                                        },

                                         'delete' => function ($url, $model) {
                                             $url1 = Url::base() . "/vendor/delete?id=" . $model->vendor_id;
                return Html::a('<span class="glyphicon glyphicon-remove"></span> Delete',$url1, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                            'data-method' => 'post', 'data-pjax' => '0',
                ]);
            },
             'category' => function ($url, $model) {
                                            $url1 =$model->vendor_id;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Food Category',
                                                            $url1,
                                                            [
                                                                'id' => $model->vendor_id,
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-success btn-xs',
                                                        'data-toggle'=> "modal",
                                                         'data-target'=> "#myModal",
                                                         'onclick'=> 'getcategory(this.id)',
                                            ]);
                                        },

                                        'product' => function ($url, $model) {
                                            $url1 =$model->vendor_id;
                                            return Html::a('<span class="glyphicon glyphicon-edit"></span> Product',
                                                            $url1,
                                                            [
                                                                'id' => $model->vendor_id,
                                                        'title' => Yii::t('app',
                                                                'Update'),
                                                        'class' => 'btn btn-info btn-xs',
                                                        'data-toggle'=> "modal",
                                                         'data-target'=> "#productModal",
                                                         'onclick'=> 'getproduct(this.id)',
                                            ]);
                                        },
                                            ],
                                        ],
                                    ],
                                ]); ?>
                             </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
<!-- ---------------modal--------------- -->

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #4CAF50;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Category Detail</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered text-center">
                <thead class="active">
                    <th>S.No.</th>
                    <th>Category Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody id="modalbody"></tbody>
            </table>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <!-- ------------------product modal----------------- -->
  <div class="modal fade" id="productModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #4CAF50;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Product Detail</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered text-center">
                <thead class="active">
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody id="productbody"></tbody>
            </table>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      function getcategory(id){
        var id =id;
        var url1 = '<?php echo Url::base() . "/foodcategory/update?id=" ?>';
        // alert(url1);
         $.ajax({
            type: "POST",
            url: "<?= Yii::$app->urlManager->createUrl('users/getcategorydata'); ?>",
            data: {id: id},
            dataType: 'html',
            cache: false,
            success: function (data) {

                var catdetail = JSON.parse(data);
               console.log(catdetail);
               var showcatdtl= " ";
            var sn = 0;
            if(catdetail!=""){
          $.each(catdetail, function(key, value){
            sn++;
            if(value.status==1){
                    var st = "Active";
                }else{
                    var st = "Inactive";
                }
            showcatdtl+="<tr><td>"+ sn +"</td><td>"+value.name+"</td><td>"+st+"</td><td><a class='btn btn-warning' href='"+url1 + value.food_category_id+"'>Edit</a></td><tr>";
          });
           }if(catdetail==""){
        showdtl+="<tr><td colspan=4; style='color:red'><h4>No Record Found</h4></td></tr> ";
      }
                    $("#modalbody").html(showcatdtl);
                  // $('#myModal').modal('hide');
                  // location.reload();

                
            },
        });
      }

       function getproduct(id){
        var id =id;
        var url1 = '<?php echo Url::base() . "/product/update?id=" ?>';
        // alert(url1);
         $.ajax({
            type: "POST",
            url: "<?= Yii::$app->urlManager->createUrl('users/getproduct'); ?>",
            data: {id: id},
            dataType: 'html',
            cache: false,
            success: function (data) {

                var productdetail = JSON.parse(data);
               console.log(productdetail);
               var showdtl= "";
            var sn = 0;
            if(productdetail!=""){
          $.each(productdetail, function(key, value){
            sn++;
            if(value.status==1){
                    var st = "Active";
                }else{
                    var st = "Inactive";
                }
            showdtl+="<tr><td>"+ sn +"</td><td>"+value.name+"</td><td>"+st+"</td><td><a class='btn btn-warning' href='"+url1 + value.product_id+"'>Edit</a></td><tr>";
          });
      }if(productdetail==""){
        showdtl+="<tr><td colspan=4; style='color:red'><h4>No Record Found</h4></td></tr> ";
      }

                    $("#productbody").html(showdtl);
                  // $('#myModal').modal('hide');
                  // location.reload();
                  // alert(showdtl);

                
            },
        });
      }
  </script>