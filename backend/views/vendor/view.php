<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\State;
use backend\models\Category;
use backend\models\Vendordocument;
use common\models\Picture;

// $model1 = new Vendordocument();

/* @var $this yii\web\View */
/* @var $model backend\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .imgbox{
        border: 1px solid black;
        margin: 5px;
        width: 200px;
        height: 200px;
    }
    .imgbox1{
        /*border: 1px solid black;*/
        /*margin: 5px;*/
        width: 150px;
        height: 170px;
    }
</style>
<div class="vendor-view">

       <section class="content-header">
        <h1 class="cbreadcrum"><?= Html::encode($this->title) ?></h1>
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                // 'vendor_id',
                                'name',
                                'email:email',
                                'mobile',
                                // 'status',
                                 ['attribute' => 'vendor_catId',
//                               'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            $catname = Category::find()->select('name')->where(['category_id'=> $model->vendor_catId])->one();
                                             if(!empty($catname)){
                                                return $catname->name;
                                            }else{
                                            return "not set";
                                        }
                                           
                                        }
                                    ],
                                // 'kyc',
//                                     ['attribute' => 'kyc',
// //                               'headerOptions' => ['style' => 'width:130px'],   
//                                         'filter' => false,
//                                         'format' => 'raw',
//                                         'value' => function($model) {
//                                             if ($model->kyc == 1) {
//                                                 return 'Yes';
//                                             } else{
//                                                 return 'No';
//                                             }
//                                         }
//                                     ],

                                'location',
                                // 'state_id',
                                'city_id',

                                ['attribute' => 'state_id',
//                               'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            $statename = State::find()->select('name')->where(['state_id'=> $model->state_id])->one();
                                            if(!empty($statename)){
                                                return $statename->name;
                                            }else{
                                            return "not set";
                                        }
                                           
                                        }
                                    ],
                                ['attribute' => 'status',
//                               'headerOptions' => ['style' => 'width:130px'],   
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($model) {
                                            if ($model->status == 1) {
                                                return 'Active';
                                            } if ($model->status == 0) {
                                                return 'Inactive';
                                            }
                                        }
                                    ],
                            ],
                        ]) ?>

 </div>
 <div class="col-xs-12 col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <h4 class="text-center">Documents</h4>
                            <hr>
                       <?php
                        $getimage = Vendordocument::find()->where(['vendorId'=>$model->vendor_id])->asArray()->all();
                       

                       foreach ($getimage as $key => $vendorimg) {
                       ?>

                        <div class="col-md-4 imgbox">
                             <img class="round imgbox1" src="<?=  Picture::getImageData(Yii::getAlias('@backend') . "/web/" . $vendorimg['documentimg'])?>" alt="image"><br/>
                             <?= $vendorimg['documentName']; ?>
                         </div>
                      <?php  } ?>

                  </div>
                         </div>
            </div>
        </div>
                     <?= Html::a('Update', ['update', 'id' => $model->vendor_id], ['class' => 'btn btn-warning'])
                                        ?>
    </section>
</div>
