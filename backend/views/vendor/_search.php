<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VendorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vendor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!-- <?= $form->field($model, 'vendor_id') ?> -->
<div class="col-lg-4">
    <?= $form->field($model, 'name') ?>
</div>

    <!-- <?= $form->field($model, 'email') ?> -->

    <!-- <?= $form->field($model, 'mobile') ?> -->

    <!-- <?= $form->field($model, 'status') ?> -->

    <?php // echo $form->field($model, 'kyc') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'state_id') ?>

    <?php // echo $form->field($model, 'city_id') ?>
<div class="col-lg-4">
    <div class="form-group" style="margin-top: 24px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default', 'id'=> 'reset']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $('#reset').click(function()

{

        location.reload()

});
</script>

