/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('.select2').select2();
    $('.bootstrap-select').multiselect({
        enableFiltering: true,
        includeSelectAllOption: true,
        buttonWidth: '100%',
        maxHeight: 250
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
    });
});
//Date picker
