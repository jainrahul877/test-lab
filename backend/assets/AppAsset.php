<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'resource/plugins/bootstrap/dist/css/bootstrap.min.css',
        'resource/font-awesome/css/font-awesome.css',
        'resource/ionicons/css/ionicons.css',
        'resource/dist/css/AdminLTE.css',
        'resource/dist/css/skins/_all-skins.min.css',
        'resource/plugins/iCheck/flat/blue.css',
        'resource/plugins/bootstrap-select/css/bootstrap-multiselect.css',
        'resource/plugins/select2/select2.css',
        'css/site.css',
        'resource/css/custom.css',
        'resource/bootstrap-botton/bootstrap-toggle.min.css',
        'resource/plugins/datepicker/datepicker3.css',
        'resource/plugins/timepicker/bootstrap-timepicker.min.css',
        'css/jquery.fancybox.min.css',
        'css/jquery.fancybox.css',
    ];
    public $js = [
        'resource/plugins/timepicker/bootstrap-timepicker.min.js',
        'resource/js/jquery-ui.min.js',
        // 'resource/js/jquery-migrate-3.0.0.min.js',
        'resource/js/jquery-migrate-1.4.0.min.js',
        'resource/plugins/bootstrap-select/js/bootstrap-multiselect.js',
        'resource/plugins/select2/select2.full.min.js',
        'resource/plugins/bootstrap/dist/js/bootstrap.min.js',
         'resource/plugins/datepicker/bootstrap-datepicker.js',
        'resource/js/common.js',
        'resource/bootstrap-botton/bootstrap-toggle.min.js',
        // 'assets/js/common.js',
        'resource/dist/js/app.min.js',
        // 'assets/js/bootstrap.min.js',
        'resource/plugins/morphing-modal-window/js/modernizr.js',
        'resource/plugins/morphing-modal-window/js/main.js',
            'resource/plugins/ckeditor/ckeditor.js',
             'resource/js/jquery.fancybox.js',
        'resource/js/jquery.fancybox.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
